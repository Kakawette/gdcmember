import React from "react";
class HistoryScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  results() {
    if (this.props.listMails.length > 0) {
      return (
        <ul>
          {this.props.listMails.map((email, index) => {
            return (
              <li key={index} className={`result ${email.status}`}>
                {email.message}
              </li>
            );
          })}
        </ul>
      );
    } else {
      return <p className="font-italic"> Aucun email testé</p>;
    }
  }

  render() {
    return (
      <div className="col-lg-6 bg-grey">
        <h3 className="text-center">Results</h3>
        <div className="log-container">{this.results()}</div>
      </div>
    );
  }
}

export default HistoryScreen;

import React from "react";

class Form extends React.Component {
  constructor(props) {
    super(props);
  }

  onKeyPressed(e) {
    if (e.keyCode === 32) {
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  sendTestEmail(e) {
    e.preventDefault();
    const data = new FormData(e.target);
    const email = data.get("textMail") + data.get("domain");
    this.props.sendEmailToAPI(email);
  }

  render() {
    return (
      <form
        className={`row ${this.props.domain}`}
        onSubmit={this.sendTestEmail.bind(this)}
      >
        <input type="hidden" name="domain" value={this.props.valueMail} />
        <div className="input-group col-lg-9">
          <input
            onKeyDown={this.onKeyPressed.bind(this)}
            className="form-control"
            name="textMail"
            placeholder="john.doe"
            type="text"
          />
          <span className="form-control input-group-addon">
            {this.props.valueMail}
          </span>
        </div>
        <div className="col-lg-3">
          <input className="btn submit-email" type="submit" value="SUBMIT" />
        </div>
      </form>
    );
  }
}
export default Form;

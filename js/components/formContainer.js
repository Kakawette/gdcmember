import React from "react";
import Form from "./form";

class FormContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="form-email col-lg-6">
        <h3 className="text-center">Enter your email address</h3>
        <div className="width-650">
          <Form
            domain="gmail"
            valueMail="@gmail.com"
            sendEmailToAPI={this.props.sendEmailToAPI}
          />
          <div className="col-lg-12 text-center">
            <p className="or">
              <span>OR</span>
            </p>
          </div>
          <Form
            domain="gdc"
            valueMail="@gensdeconfiance.fr"
            sendEmailToAPI={this.props.sendEmailToAPI}
          />
        </div>
      </div>
    );
  }
}

export default FormContainer;

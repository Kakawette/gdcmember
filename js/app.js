import "./../css/styles.scss";
import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import HistoryScreen from "./components/historyScreen";
import FormContainer from "./components/formContainer";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listMails: []
    };
  }
  sendEmailToAPI(email) {
    const apiPartnerId = "test";
    const apiPartnerSecret = "a9d131217a7c3ab9f9c2b7d43457d4a3";
    const baseUrl = "https://gensdeconfiance.fr/api/member/check.json";
    axios
      .get(
        `${baseUrl}?apiPartnerId=${apiPartnerId}&apiPartnerSecret=${apiPartnerSecret}&query=${email}`
      )
      .then(result => {
        const checkedMessage = {
          message: `${email} is a GDC member`,
          status: result.data.response.status
        };
        if (checkedMessage.status === "unknown") {
          checkedMessage.message = `BOOOH we don't know ${email}`;
        }
        this.setState({
          listMails: this.state.listMails.concat(checkedMessage)
        });
      });
  }

  render() {
    return (
      <div className="row">
        <div className="col-lg-12 header">
          <span className="pull-left">Marie BIZET</span>
          <span className="pull-right">02/11/2018</span>
        </div>
        <FormContainer sendEmailToAPI={this.sendEmailToAPI.bind(this)} />
        <HistoryScreen listMails={this.state.listMails} />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));

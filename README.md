Run app :

La première fois :
npm install

Pour lancer le serveur :
npm run start

Access :
http://localhost:8080/

Context :
"GDC exposes an API that allows to test if an email belongs to a member of our database."

Goal :
- Implement a tiny ReactJS client that allows submitting an email and checks it against this API;
- Try to make it sexy/good-looking (we will also evaluate the look and feel of your solution);
- We want to limit the usage of this tool to @gensdeconfiance.fr and @gmail.com email addresses; and the UX designer wants 1 input per domain (silly really), as shown on his proposed solution attached to this email;
- Show the result "[email] is a GDC member" / "BOOOH we don't know [email]" in a "log" window where the messages are appended so we have a history of the submitted email addresses.


![alt text](img/maquette.png)